
/*Function to get all the categories of the forum*/
//Currently using the name as id
function get_categories(callback){
    var cats = new Array();
    $.get("http://intranet.iitg.ernet.in/news/forum/12", function(data){
        
        $('.name',data).each(function(index){            
            var obj = new Object();
            obj.href= $(this).children().attr('href'); 
            obj.name=$(this).children().text();
            cats.push(obj);
                      
        }); 
         callback(cats); 
        //console.log(cats);
        
    }); 
    
}

function init() {    
     $("#loading").html("<img src='images/loader.gif' />");
     get_categories(categories_fetched);                  
}

function categories_fetched(cats)
{
    $("#loading").html("");

	var form = document.getElementById("form"),
        list = document.createElement('ul'),
        i;
    for (i = 0; i < cats.length; i++) {        
        list.appendChild(createItem(cats[i]));          
    }    
    form.appendChild(list);
}


function createItem(cat) {
    var item = document.createElement('li');
    var cb;
    cb = document.createElement( "input" );
    cb.type = "checkbox";
    cb.id = cat.name;
    cb.value = cat.name;
    cb.className = "cat-cb";
    item.appendChild(cb);
    if(localStorage.cat_prefs){
        var data = JSON.parse(localStorage.cat_prefs);
        //console.log(data);
        var theName = cb.value;
        if(data[theName].subscribed){
            cb.checked = true;
        }
    }
    text = document.createTextNode(cat.name);
    item.appendChild(text);
    
    return item;
}

function save(){
    $("#help").slideUp("fast");
    var data = new Object();
    $(".cat-cb").each(function(){ 
	var sub = 0;
        if(this.checked){
			sub = 1;
        }
		 var theName = this.value;
		 if(data[theName]){
			data[theName].subscribed=sub;
		 }
		else{
			data[theName]= {subscribed:sub,rss_feed:""};
		}
    });
    //console.log(data);
    localStorage.cat_prefs = JSON.stringify(data);
    $("#help").html("<em>Saved!</em>");
    $("#help").slideDown("slow");
	chrome.extension.getBackgroundPage().window.location.reload();
}

function select_all(){    
        $(".cat-cb").prop("checked",true);
}
function deselect_all(){            
        $(".cat-cb").prop("checked",false);
}