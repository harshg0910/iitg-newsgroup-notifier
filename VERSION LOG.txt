1.1
----------------------------------
* Initial version showing all posts.
1.12
----------------------------------
* Tiny fixes

1.3
----------------------------------
* Category wise filtering added.
* Options page added.
* UI updated.

1.4
----------------------------------
* Auto updating badge added for showing unread post count.

1.5
----------------------------------
* Sorting posts by time added.
* Feed taken from HTML pages rather than RSS
* category based filtering
* badge needs to be re-integrated

1.6
----------------------------------
* Feed updated when the options are saved.

1.7
------------------------------------------
-- Update from version 1.4: ( need to merge changes made between 1.4 and 1.6 (inclusive))

*Badge updates in realtime now
*Unlimited Caching (no limit imposed as of now)
*A placeholder for HTML feed fetched through read more button ( need santosh to add his function in
placeholder provided in background.html)
	** it's added
*Sorting posts by their node id ( not exact sorting as id's are sorted category wise)	

   *** Please see clearly. Every post irrespective of category has a unique URL-id and these r in sorted order by time.
   
* post author name added.
* Get more posts added.
* css tweaks.
1.8
------------------------------------------	
--Limit to Caching Added to v1.7